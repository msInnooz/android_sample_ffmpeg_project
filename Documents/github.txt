https://blog.csdn.net/u011686167/article/details/79157153
https://blog.csdn.net/u011686167/article/details/79146373
https://blog.csdn.net/u011686167/article/details/80726977
https://github.com/xufuji456/FFmpegAndroid


https://github.com/WritingMinds/ffmpeg-android
http://writingminds.github.io/ffmpeg-android-java/

https://blog.csdn.net/jaikydota163/article/details/78502106
1、libavformat：用于各种音视频封装格式的生成和解析，包括获取解码所需信息以生成解码上下文结构和读取音视频帧等功能，包含demuxers和muxer库；
2、libavcodec：用于各种类型声音/图像编解码；
3、libavutil：包含一些公共的工具函数；
4、libswscale：用于视频场景比例缩放、色彩映射转换；
5、libpostproc：用于后期效果处理；
6、ffmpeg：是一个命令行工具，用来对视频文件转换格式，也支持对电视卡实时编码；
7、ffsever：是一个HTTP多媒体实时广播流服务器，支持时光平移；
8、ffplay：是一个简单的播放器，使用ffmpeg 库解析和解码，通过SDL显示。
FFmpeg是一个免费的多媒体框架，可以运行音频和视频多种格式的录影、转换、流功能，能让用户访问几乎所有视频格式，包括mkv、flv、mov，VLC Media Player、Google Chrome浏览器都已经支持。
0.
Android 4.1及以上。
 支持的架构：
 ●  armv7
 ●  armv7-neon
 ●  armv8
 ●  x86
 ●  x86_64
1.引用
compile 'com.writingminds:FFmpegAndroid:0.3.2'
2.使用
FFmpeg ffmpeg = FFmpeg.getInstance(context);
try {
  ffmpeg.loadBinary(new LoadBinaryResponseHandler() 
  { 
    @Override
    public void onStart() {} 
    @Override
    public void onFailure() {} 
    @Override
    public void onSuccess() {} 
    @Override
    public void onFinish() {}
  });
} 
catch (FFmpegNotSupportedException e)
{
  // Handle if FFmpeg is not supported by device
}
3.FFmpeg命令
通过FFmpegExecuteResponseHandler接口提供回调。 
还需要将命令作为参数传递给此方法。如需要执行“ffmpeg -version”命令，只需要将“-version”作为cmd参数。
FFmpeg ffmpeg = FFmpeg.getInstance(context);
try 
{
  // to execute "ffmpeg -version" command you just need to pass "-version"
  ffmpeg.execute(cmd, new ExecuteBinaryResponseHandler()
  {
 
    @Override
    public void onStart() {}
 
    @Override
    public void onProgress(String message) {}
 
    @Override
    public void onFailure(String message) {}
 
    @Override
    public void onSuccess(String message) {}
 
    @Override
    public void onFinish() {}
  });
}
catch (FFmpegCommandAlreadyRunningException e) 
{
  // Handle if FFmpeg is already running
}
4.方法：
loadBinary(FFmpegLoadBinaryResponseHandler ffmpegLoadBinaryResponseHandler) throws FFmpegNotSupportedException
execute(Map<String, String> environvenmentVars, String cmd, FFmpegExecuteResponseHandler ffmpegExecuteResponseHandler) throws FFmpegCommandAlreadyRunningException
execute(String cmd, FFmpegExecuteResponseHandler ffmpegExecuteResponseHandler) throws FFmpegCommandAlreadyRunningException
getDeviceFFmpegVersion() throws FFmpegCommandAlreadyRunningException
getLibraryFFmpegVersion()
isFFmpegCommandRunning()
killRunningProcesses()
setTimeout(long timeout)


https://magiclen.org/android-ffmpeg/
https://segmentfault.com/a/1190000008413814
https://lnpcd.blogspot.com/2012/09/ffmpeg.html

https://github.com/yhaolpz/ffmpeg-command-ndkBuild
https://blog.csdn.net/yhaolpz/article/details/76408829

-vcodec 等同於 -c:v ，指定 視訊編碼器 或 編碼格式
-acodec 等同於 -c:a ，指定 音訊編碼器 或 編碼格式
例如使用 "-c:a aac" 會自動選擇預設的 AAC 編碼器輸出 AAC 音訊
而 -c:a libfdk_aac 則是強制指定用 fdk-aac 編碼器輸出 AAC 音訊
-c copy 或 -codec copy 會自動複製 tracks，視音訊字幕等等.....
-c:v copy 自動複製 視訊
-c:a copy 自動複製 音訊
如果要強制定 track 必須搭配 -map
https://trac.ffmpeg.org/wiki/How%20to%20use%20-map%20option

-f 則是用來指定輸出檔案格式，例如...
ffmpeg -i input.flac -c:a libfdk_aac -f rawaudio output.aac
ffmpeg -i input.flac -c:a libfdk_aac -f mpeg4 output.m4a
前者是直接輸出AAC，後者是把 AAC 封裝到 MPEG-4 內再輸出
-f 通常可以省略，ffmpeg 會自動依副檔名判斷
ffmpeg -i input.flac -c:a libfdk_aac output.aac
ffmpeg -i input.flac -c:a libfdk_aac output.m4a

不確認複寫的參數，如：
ffmpeg -y -i i.mpg -c:v libx264 -c:a aac -b:v 1M -b:a 256k -pass 1 o.mp4
ffmpeg -y -i i.mpg -c:v libx264 -c:a aac -b:v 1M -b:a 256k -pass 2 o.mp4

FFmpeg 也可以用來切割影片， -ss 是指定從哪裡開始切， -t 是從 -ss 開始切幾秒。如：
ffmpeg -i i.ts -ss 00:05:00.0 -t 00:55:00.0 o.ts
-ss 放在 -i 後面是慢速搜尋，FFmpeg 會從檔案的一開始搜尋，直到找到影格為止。

如果把指令放在前面，如下：
ffmpeg -ss 00:05:00.0 -i i.ts -t 00:55:00.0 o.ts
會用快速搜尋，FFmpeg 會直接用關鍵影格（Keyframe）搜尋，會比較快，在 FFmpeg 2.1 版後有提高精準度，可以考慮使用。 

合併影片
ffmpeg -f concat -safe 0 -i mylist.txt -c copy -map 0 o.ts
其中 mylist.txt 是：
file 'file1.ts'
file 'file2.ts'
file 'file3.ts'

FFmpeg 現在已經加入 FontConfig ，可以輕易實現字幕內嵌的功能。 
字幕轉檔：
ffmpeg -i i.mp4 -filter:v ass=i.ass o.mp4