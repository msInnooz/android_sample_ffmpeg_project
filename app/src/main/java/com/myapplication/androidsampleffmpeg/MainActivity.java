package com.myapplication.androidsampleffmpeg;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

public class MainActivity extends AppCompatActivity
{
    private static final String TAG = "TagAndroidSampleFFMpeg";
    private static final String test2 = "storage/emulated/0/sample_2.mp4";
    private static final String test3 = "storage/emulated/0/sample_3.avi";
    FFmpeg ffmpeg ;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ffmpeg = FFmpeg.getInstance(this);
    }

    private void loadFFMpegBinary()
    {
        try
        {
            ffmpeg.loadBinary(new LoadBinaryResponseHandler()
            {
                @Override
                public void onFailure()
                {
                    Log.d(TAG," ffmpeg.loadBinary onFailure");
                }
            });
        }
        catch (FFmpegNotSupportedException e)
        {
            Log.d(TAG," ffmpeg.FFmpegNotSupportedException");
        }
    }

    private void execFFmpegBinary(final String[] command)
    {
        try
        {
            ffmpeg.execute(command, new ExecuteBinaryResponseHandler()
            {
                @Override
                public void onFailure(String s)
                {
                    Log.d(TAG," ffmpeg.execute FAILED with output : "+s);
                }

                @Override
                public void onSuccess(String s)
                {
                    Log.d(TAG," ffmpeg.execute SUCCESS with output : "+s);
                }

                @Override
                public void onProgress(String s)
                {
                    Log.d(TAG, "Started command : ffmpeg "+command);
                }

                @Override
                public void onStart()
                {
                    Log.d(TAG, "Started command : ffmpeg " + command);
                }

                @Override
                public void onFinish()
                {
                    Log.d(TAG, "Finished command : ffmpeg "+command);
                }
            });
        }
        catch (FFmpegCommandAlreadyRunningException e)
        {
            Log.d(TAG, "FFmpegCommandAlreadyRunningException");
        }
    }

}
